# Globetrotter

Approximate country and language name matching for `pycountry`, making it an even better reference for messy data.

## Important Note

**Globetrotter-Provinces requires a [forked version](https://bitbucket.org/kuranes/pycountry) of pycountry that creates indexes on the 'name' field of a subdivision.**

## Motivation

```
>>> import pycountry
>>> pycountry.countries.get(name='Vietnam')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/Users/you/Library/Python/2.7/lib/python/site-packages/pycountry/db.py", line 85, in get
    return self.indices[field][value]
KeyError: 'Vietnam'
>>>
>>> pycountry.subdivisions.get(name='LasTunas')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/Users/you/Library/Python/2.7/lib/python/site-packages/pycountry/db.py", line 87, in get
    return self.indices[field][value]
KeyError: 'LasTunas'
>>> 
```

'Nuff said.

## Installation

```
pip install -e git+https://bitbucket.org/kuranes/globetrotter-provinces#egg=globetrotter-provinces
```

## Usage

Find a country through the `find_country()` method, giving it an approximate string name. It returns a `Country` object with ISO metadata about the match.

```
>>> import globetrotter
>>> globetrotter.find_country('Vietnam')
<pycountry.db.Country object at 0x1038eed90>
>>> globetrotter.find_country('Vietnam').name
u'Viet Nam'
```

Find a language through the `find_language()` method in a similar way. It returns a `Language` object.

```
>>> import globetrotter
>>> globetrotter.find_language('Spanish')
<pycountry.db.Language object at 0x103a444d0>
>>> globetrotter.find_language('Spanish').name
u'Spanish; Castilian'
```

Find a subdivision through the `find_subdivision()` method in a similar way. It returns a `Subdivision` object.

```
>>> import globetrotter
>>> globetrotter.find_subdivision('LasTunas')
<pycountry.db.Subdivision at 0xbb71a2c>
>>> globetrotter.find_subdivision('LasTunas').name
u'Las Tunas'
```